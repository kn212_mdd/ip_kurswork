<?php

namespace models;

use core\Utils;

class CategoryModel
{
    protected static $tableName = 'category';

    public static function AddCategory($name, $photoPath)
    {
        do {
            $fileName = uniqid() .'.jpg';
            $newPath = "files/category/{$fileName}";
        } while (file_exists($newPath));
        move_uploaded_file($photoPath, $newPath);
        \core\Core::getInstance()->getDB()->insert(self::$tableName, [
            'name' => $name,
            'photo' => $fileName
        ]);
    }

    public static function GetCategoryById($id)
    {
        $rows = \core\Core::getInstance()->getDB()->select(self::$tableName, "*", [
            'id' => $id
        ]);
        if (!empty($rows))
            return $rows[0];
        else
            return null;
    }

    public static function DeleteCategory($id)
    {
        self::deletePhotoFile($id);
        \core\Core::getInstance()->getDB()->delete(self::$tableName, [
            'id' => $id
        ]);
    }

    public static function UpdateCategory($id, $newName)
    {
        \core\Core::getInstance()->getDB()->update(self::$tableName, [
            'name' => $newName
        ], [
            'id' => $id
        ]);
    }

    public static function deletePhotoFile($id){
        $row=self::GetCategoryById($id);
        $photoPath = 'files/category/'.$row['photo'];
        if(is_file($photoPath))
            unlink($photoPath);
    }

    public static function changePhoto($id,$newPhoto){
        self::deletePhotoFile($id);
        do {
            $fileName = uniqid() .'.jpg';
            $newPath = "files/category/{$fileName}";

        } while (file_exists($newPath));
        move_uploaded_file($newPhoto, $newPath);
        \core\Core::getInstance()->getDB()->update(self::$tableName, [
            'photo' => $fileName
        ], [
            'id' => $id
        ]);
    }
    public static function GetCategories()
    {
        $rows= \core\Core::getInstance()->getDB()->select(self::$tableName);
        return $rows;
    }
}

