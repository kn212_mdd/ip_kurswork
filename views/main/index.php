<?php
/** @var array $rows */

?>
<main>
    <section class="py-5 text-center container" style="background-color: lightgray">
        <div class="row py-lg-5">
            <div class="col-lg-6 col-md-8 mx-auto">
                <h1 class="fw-light">Вітаємо в інтернет-магазині <b>"Дірявий казан"</b></h1>
                <p class="lead text-muted">Тут можна замовити багато відьомських речей</p>
                <p>
                    <a href="/product" type="button" class="btn btn-dark my-2">Товари</a>
                    <a href="/category" type="button"  class="btn btn-dark my-2">Категорії</a>
                </p>
            </div>
        </div>
    </section>
    <div class="album py-5 bg-light">
        <div class="container">

            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
                <div class="col">
                    <div class="card shadow-sm">
                        <img src="/files/category/Свічка.JPG" class="card-img-top " alt="">
                        <rect width="100%" height="100%" fill="#55595c"></rect>
                        </svg>
                        <div class="card-body">
                            <p class="card-text">Великий вибір свічок</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <small class="text-muted">9 mins</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card shadow-sm">
                        <img src="/files/category/Амулет.JPG" class="card-img-top " alt="">
                        </svg>
                        <div class="card-body">
                            <p class="card-text">Багато різноманітних амулетів</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <small class="text-muted">10 mins</small>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col">
                    <div class="card shadow-sm">
                        <img src="/files/category/Браслет.JPG" class="card-img-top " alt="">
                        </svg>
                        <div class="card-body">
                            <p class="card-text">Чарівні браслети ручної роботи</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <small class="text-muted">9 mins</small>
                            </div>
                        </div>
                    </div>
                </div>

                <a class="btn btn-dark centered mt-5 mx-auto" href="/product">До товарів!</a>
            </div>
        </div>
    </div>
    </div>

</main>
<section class="">
    <footer class="text-center text-white" style="background-color: gray">
        <div class="container p-4 pb-0">
            <section class="">
                <p class="d-flex justify-content-center align-items-center">
                    <span class="me-3">Реєстрація</span>
                    <a type="button" href="/users/register" class="btn btn-outline-light btn-rounded">
                        Зареєструватись!
                    </a>
                </p>
            </section>
        </div>
        <div class="text-center p-3" style="background-color: dimgray">
            © 2020 Copyright:
            <a class="text-white" href="https://mdbootstrap.com/">MDBootstrap.com</a>
        </div>
    </footer>
</section>
<script src="/docs/5.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>




