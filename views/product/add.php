<?php
/** @var array $errors */
/** @var array $model */
/** @var array $categories */
/** @var array $category_id */
?>

<form action="" method="post" enctype="multipart/form-data">
    <div class="mb-3">
        <label for="name" class="form-label">Назва товару</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="Назва">
        <?php if (!empty($errors['name'])): ?>
            <div class="form-text text-danger "> <?= $errors['name']; ?></div>
        <?php endif; ?>
    </div>
    <div class="mb-3">
        <label for="exampleFormControlInput1" class="form-label">Виберіть категорію товару</label>
        <select class="form-control" id="category_id" name="category_id">
            <?php foreach ($categories as $category) : ?>
                <option <?php if($category['id']==$category_id)?> value="<?= $category['id'] ?>"><?= $category['name'] ?></option>
            <?php endforeach; ?>
        </select>
        <?php if (!empty($errors['category_id'])): ?>
            <div class="form-text text-danger "> <?= $errors['category_id']; ?></div>
        <?php endif; ?>
    </div>


    <div class="mb-3">
        <label for="price" class="form-label">Ціна товару</label>
        <input type="number" class="form-control" id="price" name="price" placeholder="Ціна">
        <?php if (!empty($errors['price'])): ?>
            <div class="form-text text-danger "> <?= $errors['price']; ?></div>
        <?php endif; ?>
    </div>
    <div class="mb-3">
        <label for="count" class="form-label">Кількість одиниць товару</label>
        <input type="number" class="form-control" id="count" name="count" placeholder="Кількість">
        <?php if (!empty($errors['count'])): ?>
            <div class="form-text text-danger "> <?= $errors['count']; ?></div>
        <?php endif; ?>
    </div>
    <div class="mb-3">
        <label for="text" class="form-label">Короткий опис товару</label>
        <textarea class="form-control ckeditor " name="short_text" id="short_text" placeholder="Опис"></textarea>
        <?php if (!empty($errors['short_text'])): ?>
            <div class="form-text text-danger "> <?= $errors['short_text']; ?></div>
        <?php endif; ?>
    </div>

    <div class="mb-3">
        <label for="visible" class="form-label">Чи відображати товар</label>
        <select class="form-control" id="visible" name="visible">
            <option value="1">Так</option>
            <option value="0">Ні</option>
        </select>
        <?php if (!empty($errors['visible'])): ?>
            <div class="form-text text-danger "> <?= $errors['visible']; ?></div>
        <?php endif; ?>

    </div>


    <div class="mb-3">
        <label for="file" class="form-label">Файл з фотограцією для товару</label>
        <input multiple type="file" class="form-control" id="file" name="file" accept="image/jpeg"/>
    </div>
    <div>
        <button class="btn btn-dark mb-2" type="submit">Додати</button>
    </div>
</form>

<script src="https://cdn.ckeditor.com/ckeditor5/35.3.1/classic/ckeditor.js"></script>
<script>
    ClassicEditor
        .create( document.querySelector( '.ckeditor' ) )
        .catch( error => {
            console.error( error );
        } );
</script>


