<?php
/** @var array $model */

?>

<div class="alert alert-danger" role="alert">
    <h4 class="alert-heading">Видалити товар "<?=$model[0]['name']?>"</h4>
    <hr>
    <p class="mb-0">
        <a href="/product/delete?id=<?=$model[0]['id']?>&confirm=yes" class="btn btn-danger">Видалити</a>
        <a href="/product" class="btn btn-success">Відмінити</a>
    </p>
</div>
