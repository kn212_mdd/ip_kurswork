<?php
/** @var array $rows */

?>

<h2>Список категорій</h2>
<?php
$userModel = new models\Users();

if ($userModel->IsAdmin()) :; ?>
    <div class="mb-3">
        <a href="/category/add" class="btn btn-success">Додати категорію</a>
    </div>
<?php endif; ?>
<div class="row row-cols-1 row-cols-md-4 g-4 ">
    <?php foreach ($rows as $row) : ?>
        <div class="categories-list">
            <div class="col">
                <a href="/category/view?id=<?=$row['id']?>">
                    <div class="card">
                        <?php
                        $filePath = 'files/category/' . $row['photo']; ?>
                        <?php if (is_file($filePath)) : ?>
                            <img src="/<?= $filePath ?>" class="card-img-top " alt="...">
                        <?php else : ?>
                            <img src="/image/no_image.jpg" class="card-img-top " alt="...">
                        <?php endif; ?>
                        <div class="card-body  mx-auto">
                            <h5 class="card-title"><?= $row['name'] ?></h5>
                        </div>
                        <?php
                        if ($userModel->IsAdmin()) :; ?>
                            <div class="card-body ">
                                <a class="btn btn-primary" href="/category/edit?id=<?= $row['id'] ?>">Редагувати</a>
                                <a class="btn btn-danger mx-auto" href="/category/delete?id=<?= $row['id'] ?>">Видалити</a>
                            </div>
                        <?php endif; ?>
                    </div>
                </a>
            </div>
        </div>

    <?php endforeach; ?>
</div>




