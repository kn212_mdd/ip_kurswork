<?php

namespace controllers;

use core\Controller;

class Main extends Controller
{
    public function actionIndex(){
        $result = [
            'Title' =>'Заголовок',
            'Content'=>'Контент'

        ];
        return $this->render('index',null,[
            'MainTitle' => 'Головна сторінка',
        ]);
    }
}
